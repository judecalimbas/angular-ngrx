import { createAction, props } from '@ngrx/store';

export enum MessageActionTypes {
  LOAD_MESSAGES = '[Message] Load Messages',
  LOAD_MESSAGES_SUCCESS = '[Message] Load Messages Success',
  LOAD_MESSAGES_FAILURE = '[Message] Load Messages Failure',
  ADD_MESSAGE = '[Message] Add Message',
}

export const loadMessages = createAction(
  MessageActionTypes.LOAD_MESSAGES
);

export const loadMessagesSuccess = createAction(
  MessageActionTypes.LOAD_MESSAGES_SUCCESS,
  props<{ payload: any }>()
);

export const loadMessagesFailure = createAction(
  MessageActionTypes.LOAD_MESSAGES_FAILURE,
  props<{ error: any }>()
);

export const addMessage = createAction(
  MessageActionTypes.ADD_MESSAGE,
  props<{ payload: { content: any } }>()
)
