import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { selectMessages } from '../reducers/message.reducer';
import {Message} from '../models/message';
import {State} from '../reducers';
import {addMessage, loadMessages} from '../actions/message.actions';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-dashhboard',
  templateUrl: './dashhboard.component.html',
  styleUrls: ['./dashhboard.component.css']
})
export class DashhboardComponent implements OnInit {
  messages$: Observable<Message[]>
  messageForm: any

  constructor(private store: Store<State>, private formBuilder: FormBuilder) {
    this.messages$ = store.pipe(select(selectMessages))
    this.messageForm = this.formBuilder.group({
      content: ''
    })
  }

  submit(data: { content: string }) {
    this.store.dispatch(addMessage({ payload: data }))
    if (this.messageForm.valid) {
      this.messageForm.reset();
    }
  }

  ngOnInit(): void {
    this.store.dispatch(loadMessages());
  }

}
