import {HttpClient} from '@angular/common/http'
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor (private http: HttpClient) {}

  getAll() {
    return this.http.get("http://dummy.restapiexample.com/api/v1/employees")
  }
}
