import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {MessageState, messageReducer} from './message.reducer';


export interface State {
  messageState: MessageState
}

export const reducers: ActionReducerMap<State> = {
  messageState: messageReducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
