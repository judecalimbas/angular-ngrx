import { Message } from '../models/message'
import {MessageActionTypes} from '../actions/message.actions'
import {createSelector} from '@ngrx/store'
import {State} from '.'

export interface MessageState {
  messages: Message[]
}

const initialState: MessageState = {
  messages: []
}

export const messageReducer = (state = initialState, action: any): MessageState  => {
  switch (action.type) {
    case MessageActionTypes.ADD_MESSAGE:
      return { ...state, messages: [...state.messages, (new Message({ content: action.payload.content }))] }
    case MessageActionTypes.LOAD_MESSAGES_SUCCESS:
      return { ...state, messages: [ ...state.messages, ...action.payload ] }
    default:
      return state
  }
}

export const selectMessageState = (state: State) => state.messageState
export const selectMessages = createSelector(selectMessageState, (state: MessageState) => state.messages)
