import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashhboardComponent } from './dashhboard/dashhboard.component'

const routes: Routes = [
  { path: '', component: DashhboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
