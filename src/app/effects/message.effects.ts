import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {MessageActionTypes, loadMessagesSuccess} from '../actions/message.actions';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { MessageService } from '../services/messages.service';
import {EMPTY} from 'rxjs';
import {Message} from '../models/message';

@Injectable()
export class MessageEffects {
  loadMessages$ = createEffect(() => this.actions$.pipe(
    ofType(MessageActionTypes.LOAD_MESSAGES),
    mergeMap(() => this.messageService.getAll().pipe(
      map(messages => (loadMessagesSuccess({ payload: [new Message({content: 'old message'})] }))),
      catchError(() => EMPTY)
    ))
  ));

  constructor(private actions$: Actions, private messageService: MessageService) {}
}
